
import bisect
import itertools
import math
import numpy as np


class RBMDist:
    """
    """

    def __init__(self, **kwargs):
        # Model parameters
        self.num_hidden = kwargs.get('num_hidden') or kwargs['c'].shape[0]
        self.num_visible = kwargs.get('num_visible') or kwargs['b'].shape[0]
        self.b = kwargs.get('b', np.random.normal(size=self.num_visible))
        self.c = kwargs.get('c', np.random.normal(size=self.num_hidden))
        self.W = kwargs.get('W', np.random.normal(size=(self.num_hidden, self.num_visible)))

    def query_latent(self, v):
        """
        h is an n-dim vec; v is a m-dim vec
        @return scalar denoting p(h | v)

        """
        prob = sigmoid(np.dot(self.W, v) + self.c)
        return np.floor(prob + np.random.uniform(0, 1, prob.shape[0]))

    def query_visible(self, h):
        """
        h is an n-dim vec; v is a m-dim vec
        @return scalar denoting p(v | h)
        """
        prob = sigmoid(np.dot(self.W.T, h) + self.b)
        return np.floor(prob + np.random.uniform(0, 1, prob.shape[0]))

    def max_post_h(self, V):
        return (np.matmul(self.W, V.T).T + self.c) > 0.5

    def max_post_v(self, H):
        return (np.matmul(self.W.T, H.T).T + self.b) > 0.5


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


class RBMOptimizer:
    """
    Optimizes the parameters of RBM distribution
    """

    def __init__(self, rbm_dist, **kwargs):

        self.rbm_dist = rbm_dist
        self.K = kwargs.get('K') or 5
        self.sampler = GibbsRBMSampler(self.rbm_dist)

    def run(self, V, **train_params):
        epochs = train_params.get('epochs') or 10
        eta = train_params.get('lr') or 0.01
        niter = 1
        interv = int((V.shape[0] * epochs) / 64)
        v1t_samples = []
        v1 = V[0]
        for i in range(epochs):
            for v in V:
                vk = self.sampler.generate_vk(v, self.K)
                term = sigmoid(np.dot(self.rbm_dist.W, v) + self.rbm_dist.c)
                term_tilde = sigmoid(np.dot(self.rbm_dist.W, vk) + self.rbm_dist.c)

                # update params
                self.rbm_dist.b += eta * (v - vk)
                self.rbm_dist.c += eta * (term - term_tilde)
                update = eta * (np.outer(term, v) - np.outer(term_tilde, vk))
                self.rbm_dist.W += update

                if niter % interv == 0:
                    v1k = self.sampler.generate_vk(v1, self.K)
                    v1t_samples.append(v1k)

                niter += 1
            H = self.rbm_dist.max_post_h(V)
            Vt = self.rbm_dist.max_post_v(H)
            print("Epoch {} Difference: {}".format(i, np.mean(np.sum(np.square(V-Vt), axis=1))))

        np.save("./samples.npy", v1t_samples)


class GibbsRBMSampler:

    def __init__(self, rbm_dist, block_size=1):
        self.rbm_dist = rbm_dist
        self.block_size = block_size
        self.size_h = self.rbm_dist.num_hidden
        self.size_v = self.rbm_dist.num_visible


    def generate_vk(self, v0, k):
        v = v0
        for i in range(k):
            h = self.rbm_dist.query_latent(v)
            v = self.rbm_dist.query_visible(h)
        return v

