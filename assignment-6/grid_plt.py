
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

from dataops import load_labelled_data


X, _ = load_labelled_data("./data/train_trunc.csv")

v1 = np.copy(X[0])

plt.imshow(np.array(v1).reshape(28,28), cmap=cm.gray)


v1samples = np.load('samples.npy')
print(v1samples.shape)
v1samples1 = []

final_image = []

for i in range(v1samples.shape[0]):
    v1samples1.append(v1samples[i].reshape(28, 28))

print(len(v1samples))

for i in range(8):
    final_image.append(np.hstack(v1samples1[8*i:8*(i+1)]))

final_image = np.concatenate(final_image, axis=0)

# plt.imshow(final_image, cmap=cm.gray)
plt.show()
