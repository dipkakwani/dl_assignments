#from sklearn.manifold import TSNE
from MulticoreTSNE import MulticoreTSNE as TSNE
from matplotlib import pyplot as plt

class Plot:
    def __init__(self):
        pass

    def plot(self, X, Y, file_name):
        #tsne = TSNE(n_iter=250)
        tsne = TSNE(n_jobs=8, n_iter=500)
        X_2d = tsne.fit_transform(X)
        target_names = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        target_ids = range(len(target_names))

        fig = plt.figure(figsize=(6, 5))
        colors = 'r', 'g', 'b', 'c', 'm', 'y', 'k', 'deeppink', 'orange', 'purple'
        for i, c, label in zip(target_ids, colors, target_names):
            plt.scatter(X_2d[Y == i, 0], X_2d[Y == i, 1], c=c, label=label)
        plt.legend()
        #plt.show()
        fig.savefig(file_name + ".png")

