import numpy as np

def load_labelled_data(path):
    """
    Load labelled data in binary form.
    """
    X = np.loadtxt(path, dtype=np.uint8, delimiter=',', skiprows=1)
    Y = X[:, -1]
    X = X[:, 1:-1]
    X = np.where(X < 127, 0, 1)
    return X, Y
