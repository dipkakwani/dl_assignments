
import bisect
import itertools
import math
import numpy as np


class RBMDist:
    """
    """

    def __init__(self, **kwargs):
        # Model parameters
        self.num_hidden = kwargs.get('num_hidden') or kwargs['c'].shape[0]
        self.num_visible = kwargs.get('num_visible') or kwargs['b'].shape[0]
        self.b = kwargs.get('b', np.random.normal(size=self.num_visible))
        self.c = kwargs.get('c', np.random.normal(size=self.num_hidden))
        self.W = kwargs.get('W', np.random.normal(size=(self.num_hidden, self.num_visible)))

    def query_latent(self, h, v):
        """
        h is an n-dim vec; v is a m-dim vec
        @return scalar denoting p(h | v)

        Due to broadcasting rules, this also works when H is matrix
        of p hidden vectors
        """
        prob = sigmoid(np.dot(self.W, v) + self.c)
        # return np.sum(np.log((h * prob + (1 - h) * (1 - prob))), axis=1)
        return np.product(h * prob + (1 - h) * (1 - prob), axis=1)

    def query_visible(self, h, v):
        """
        h is an n-dim vec; v is a m-dim vec
        @return scalar denoting p(v | h)
        """
        prob = sigmoid(np.dot(self.W.T, h) + self.b)
        # return np.sum(np.log(v * prob + (1 - v) * (1 - prob)), axis=1)
        return np.product(v * prob + (1 - v) * (1 - prob), axis=1)

    def max_post_h(self, V):
        return (np.matmul(self.W, V.T).T + self.c) > 0.5

    def max_post_v(self, H):
        return (np.matmul(self.W.T, H.T).T + self.b) > 0.5


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def normalize(l):
    s = sum(l)
    norm = [float(i)/s for i in l]
    return norm


class RBMOptimizer:
    """
    Optimizes the parameters of RBM distribution
    """

    def __init__(self, rbm_dist):

        self.rbm_dist = rbm_dist
        self.K = 1
        self.eta = 0.1

        # initialize the params of RBM dist
        self.rbm_dist.b = np.random.normal(size=self.rbm_dist.num_visible)
        self.rbm_dist.c = np.random.normal(size=self.rbm_dist.num_hidden)
        self.rbm_dist.W = np.random.normal(size=(self.rbm_dist.num_hidden, self.rbm_dist.num_visible))
        # self.rbm_dist.b = np.zeros(self.rbm_dist.num_visible)
        # self.rbm_dist.c = np.zeros(self.rbm_dist.num_hidden)
        # self.rbm_dist.W = np.zeros((self.rbm_dist.num_hidden, self.rbm_dist.num_visible))

        self.sampler = GibbsRBMSampler(self.rbm_dist)

    def run(self, train):
        epochs = 1
        for i in range(epochs):
            print("EPOCH ", i)
            for v in train:
                vk = self.sampler.generate_vk(v, self.K)
                term = sigmoid(np.dot(self.rbm_dist.W, v) + self.rbm_dist.c)
                term_tilde = sigmoid(np.dot(self.rbm_dist.W, vk) + self.rbm_dist.c)

                # update params
                self.rbm_dist.b += self.eta * (v - vk)
                self.rbm_dist.c += self.eta * (term - term_tilde)
                self.rbm_dist.W += self.eta * ((term[:, np.newaxis] * v[np.newaxis, :]) - (term_tilde[:, np.newaxis] * vk[np.newaxis, :]))


class GibbsRBMSampler:

    def __init__(self, rbm_dist, block_size=1):
        self.rbm_dist = rbm_dist
        self.block_size = block_size
        self.size_h = self.rbm_dist.num_hidden
        self.size_v = self.rbm_dist.num_visible

    def generate_vk(self, v0, K):
        samples = []    # a list of (h, v)

        # block domain
        domain = list(itertools.product([0, 1], repeat=self.block_size))
        domain = np.array(domain)
        card = domain.shape[0]

        v, h = v0, np.zeros(self.size_h)
        parts_h = math.ceil(self.size_h/self.block_size)
        parts_v = math.ceil(self.size_v/self.block_size)

        for t in range(K):
            # sample h
            for num in range(parts_h):
                H = np.repeat(h[np.newaxis, :], card, axis=0)
                bstart = num * self.block_size
                bend = (num+1) * self.block_size
                H[:, bstart:bend] = domain
                probs = self.rbm_dist.query_latent(H, v)
                probs = normalize(probs)
                cum_probs = np.cumsum(probs)
                rand_num = np.random.uniform()
                index = bisect.bisect_left(cum_probs, rand_num)
                h = H[index]
            # sample v
            for num in range(parts_h):
                V = np.repeat(v[np.newaxis, :], card, axis=0)
                bstart = num * self.block_size
                bend = (num+1) * self.block_size
                V[:, bstart:bend] = domain
                probs = self.rbm_dist.query_visible(h, V)
                probs = normalize(probs)
                cum_probs = np.cumsum(probs)
                rand_num = np.random.uniform()
                index = bisect.bisect_left(cum_probs, rand_num)
                v = V[index]
        return v
