#!/usr/bin/env bash

python3 main.py --lr 0.1 \
                 --batch_size 128 --epochs 5 \
                 --train data/train.csv \
                 --test data/test.csv \
