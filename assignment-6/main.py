import argparse
from dataops import *
from RBM1 import *
from plot import *

def parse_args():
    """
    Parses and processes the arguments obtained from the command
    lines and returns it as a dictionary.
    """
    parser = argparse.ArgumentParser()
    req_named_args = parser.add_argument_group("required named arguments")
    req_named_args.add_argument("--lr", required=True)
    req_named_args.add_argument("--batch_size", required=True)
    req_named_args.add_argument("--epochs", required=True)
    req_named_args.add_argument("--train", required=True)
    req_named_args.add_argument("--test", required=True)

    args = vars(parser.parse_args())

    args["lr"]         = float(args["lr"])
    args["batch_size"] = int(args["batch_size"])
    args["epochs"]     = int(args["epochs"])

    return args



def main():
    gparams = parse_args()
    # gparams = {'train': './data/train_trunc.csv', 'test': './data/test.csv'}
    train, y = load_labelled_data(gparams["train"])
    test, _ = load_labelled_data(gparams["test"])
    rbm_dist = RBMDist(num_visible=train.shape[1], num_hidden=100)
    optimizer = RBMOptimizer(rbm_dist, **gparams)
    optimizer.run(train, **gparams)

    H = rbm_dist.max_post_h(train)
    plt = Plot()
    plt.plot(X=H, Y=y, file_name="hidden")
    plt.plot(X=train, Y=y, file_name="visible")



if __name__ == '__main__':
    main()
