
import numpy as np
from dllib.dataops import read_dataset, write_dataset

X1, Y1 = read_dataset("data/train.csv")
X2, Y2 = read_dataset("data/valid.csv")

X = np.concatenate((X1, X2), axis=0)
Y = np.concatenate((Y1, Y2), axis=0)

write_dataset("data/train+valid.csv", X, Y)

