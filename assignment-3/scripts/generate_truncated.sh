#!/usr/bin/env bash

# All of these variables must be less than 10k
train_sz=10000
valid_sz=1000
test_sz=1000


##  For training set

# Write the header to the output file
head -1 train.csv > train_trunc.csv
# Append randomly picked `train_sz` lines to the output file
cat train.csv    |
    awk 'NR > 1' |
    shuf         |
    head -n $train_sz >> train_trunc.csv


## For validation set

# Write the header to the output file
head -1 valid.csv > valid_trunc.csv
# Append randomly picked `train_sz` lines to the output file
cat valid.csv      |
    awk 'NR > 1' |
    shuf         |
    head -n $valid_sz >> valid_trunc.csv


## For test set

# Write the header to the output file
head -1 test.csv > test_trunc.csv
# Append randomly picked `train_sz` lines to the output file
cat test.csv     |
    awk 'NR > 1' |
    shuf         |
    head -n $test_sz >> test_trunc.csv

