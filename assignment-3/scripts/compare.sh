#!/usr/bin/env bash


l=$(awk 'NR==FNR{a[$0];next}(!($0 in a)){print}' $1 $2 | wc -l) 
echo "Number of lines different $l"
awk 'NR==FNR{a[$1];next} $1 in a{print $1}' $1 $2 > similarity.csv
awk -F ',' 'NR==FNR{a[$0];next}(!($0 in a)){print}' $1 $2 > diff12.csv
awk -F ',' 'NR==FNR{a[$0];next}(!($0 in a)){print}' $2 $1 > diff21.csv
echo "id,label-$1,label-$2" > dissimlarity.csv
paste -d',' diff21.csv <(cut -d',' -f 2 diff12.csv) >> dissimlarity.csv
rm diff12.csv diff21.csv

