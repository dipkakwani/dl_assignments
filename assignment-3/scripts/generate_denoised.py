
import numpy as np
from scipy.ndimage import median_filter
from PIL import Image
from dllib.dataops import read_dataset, write_dataset


def process_x(x):
    arr = x.reshape(28, 28)
    im = Image.fromarray(arr, 'L')
    im = median_filter(im, size=3)
    im = im.flatten()
    return im


X, Y = read_dataset("data/train.csv", True, np.uint8)
size = X.shape[0]

for index in range(size):
    print(index)
    X[index, :] = process_x(X[index, :])

write_dataset("data/train_processed.csv", X, Y)

