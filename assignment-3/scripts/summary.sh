
# Prints the counts of examples in each class
cat ../data/train_trunc.csv  | awk -F, 'NR > 1{ print $786 }' | sort | uniq -c
cat ../data/valid_trunc.csv  | awk -F, 'NR > 1{ print $786 }' | sort | uniq -c
cat ../data/valid.csv  | awk -F, 'NR > 1{ print $786 }' | sort | uniq -c

