
## TODOs

* Implement serialization - Diptanshu, before wednesday
    - add a method in NeuralNet class -- DONE
* Generate logs - Diptanshu, before wednesday
    - Pick up the debug parameter in Optimizer::__init__ and add a
      write statement in the Optimizer::run method -- DONE
* Implement MSE
    - Leave it for later
* Write report - sit together on wednesday
    - Make plot library inside dllib
    - put specific plot code in generate_plots.py

Divyanshu - by Wednesday
* Implement early stopping -- DONE
* Implement checkpointing - save the model parameters when the least
validation error has been acheived -- DONE
* Train on both train + valid -- TESTED


Diptanshu - by Wednesday
* Implement bagging - train two models - one on noisy and and one on
noiseless data. Then take the average of the outputs - Diptanshu
