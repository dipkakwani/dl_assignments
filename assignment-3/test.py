
import argparse
import itertools
import json
import copy
import sys
from dllib.dataops import read_split_dataset, standardize_dataset
from dllib.neuralnet import NeuralNet
from dllib.optimizers import get_optimizer
from dllib.utils import write_kaggle_submission



def parse_args():
    """
    Parses and processes the arguments obtained from the command
    lines and returns it as a dictionary.
    """
    parser = argparse.ArgumentParser()
    req_named_args = parser.add_argument_group("required named arguments")
    req_named_args.add_argument("--lr", required=True)
    req_named_args.add_argument("--momentum", required=True)
    req_named_args.add_argument("--num_hidden", required=True)
    req_named_args.add_argument("--sizes", required=True)
    req_named_args.add_argument("--activation", required=True)
    req_named_args.add_argument("--loss", required=True)
    req_named_args.add_argument("--opt", required=True)
    req_named_args.add_argument("--batch_size", required=True)
    req_named_args.add_argument("--epochs", required=True)
    req_named_args.add_argument("--anneal", required=True)
    req_named_args.add_argument("--save_dir", required=True)
    req_named_args.add_argument("--expt_dir", required=True)
    req_named_args.add_argument("--train", required=True)
    req_named_args.add_argument("--val", required=True)
    req_named_args.add_argument("--test", required=True)

    opt_named_args = parser.add_argument_group('optional arguments')
    opt_named_args.add_argument("--debug", required=False)
    opt_named_args.add_argument("--pretrain", required=False)
    opt_named_args.add_argument("--state", required=False)
    opt_named_args.add_argument("--testing", required=False)


    args = vars(parser.parse_args())

    # Cast the arguments into the required types
    args["lr"]         = float(args["lr"])
    args["momentum"]   = float(args["momentum"])
    args["num_hidden"] = int(args["num_hidden"])
    args["batch_size"] = int(args["batch_size"])
    args["epochs"]     = int(args["epochs"])
    args["anneal"]     = (args["anneal"] == "True" or args["anneal"] == "true")
    args["sizes"]      = [int(size) for size in args["sizes"].split(",")]
    args["pretrain"]   = (args["pretrain"] == "True" or args["pretrain"] == "true")
    args["state"]      = 0 if args["state"] is None or not args["pretrain"] else int(args["state"])
    args["testing"]    = (args["testing"] == "True" or args["testing"] == "true")
    args["debug"]      = (args["debug"] != "false")


    lr_list = [0.001]

    """
    base_sizes = [400, 300, 200, 100]
    #base_sizes = [300, 200]
    sizes_list = []
    for i in range(2, 4):
        for combination in itertools.combinations(base_sizes, i):
            sizes_list.append(list(combination))
   """
    sizes_list = [[400, 100], [400, 200]]
    batch_sizes_list = [32, 64, 128]
    #batch_sizes_list = [32, 64]

    args_list = []
    for params in itertools.product(lr_list, sizes_list, batch_sizes_list):
        new_args = copy.deepcopy(args)
        new_args["lr"] = params[0]
        new_args["num_hidden"] = len(params[1])
        new_args["sizes"] = params[1]
        new_args["batch_size"] = params[2]
        print(params)
        args_list.append(new_args)


    print(len(args_list))

    return args_list



def main():
    gparams_list = parse_args()
    for i in range(len(gparams_list)):
        print("Runnning " + str(i) + " experiment")
        split_ds = read_split_dataset(**gparams_list[i])
        split_ds = standardize_dataset(split_ds)

        net = NeuralNet(**gparams_list[i])
        Optimizer = get_optimizer(gparams_list[i]['opt'])
        optimizer = Optimizer(net, split_ds, **gparams_list[i])

        if not gparams_list[i]["testing"]:
            optimizer.run(gparams_list[i]['batch_size'], gparams_list[i]['epochs'], gparams_list[i]['state'])

        # Run the model on the test data
        #Yhat = net.forward(split_ds.test.X)
        #write_kaggle_submission("predictions_" + str(i), Yhat)

        # Dump input parameters and accuracy
        with open('logfile' + str(i), 'w') as file:
            file.write(json.dumps(gparams_list[i]) + "\n")
            J_train = optimizer._compute_loss(split_ds.train)
            J_valid = optimizer._compute_loss(split_ds.valid)
            J_train_accuracy = optimizer._compute_accuracy(split_ds.train)
            J_valid_accuracy = optimizer._compute_accuracy(split_ds.valid)
            file.write("Loss at the end: Train = {}; Valid = {};\n"\
                  .format(J_train, J_valid))
            file.write("Accuracy at the end: Train = {}; Valid = {};\n"\
                  .format(J_train_accuracy, J_valid_accuracy))




if __name__ == '__main__':
    main()