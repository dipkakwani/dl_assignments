
import argparse
from dllib.dataops import read_split_dataset, standardize_dataset
from dllib.neuralnet import NeuralNet
from dllib.optimizers import get_optimizer
from dllib.utils import write_kaggle_submission



def parse_args():
    """
    Parses and processes the arguments obtained from the command
    lines and returns it as a dictionary.
    """
    parser = argparse.ArgumentParser()
    req_named_args = parser.add_argument_group("required named arguments")
    req_named_args.add_argument("--lr", required=True)
    req_named_args.add_argument("--momentum", required=True)
    req_named_args.add_argument("--num_hidden", required=True)
    req_named_args.add_argument("--sizes", required=True)
    req_named_args.add_argument("--activation", required=True)
    req_named_args.add_argument("--loss", required=True)
    req_named_args.add_argument("--opt", required=True)
    req_named_args.add_argument("--batch_size", required=True)
    req_named_args.add_argument("--epochs", required=True)
    req_named_args.add_argument("--anneal", required=True)
    req_named_args.add_argument("--save_dir", required=True)
    req_named_args.add_argument("--expt_dir", required=True)
    req_named_args.add_argument("--train", required=True)
    req_named_args.add_argument("--val", required=True)
    req_named_args.add_argument("--test", required=True)

    opt_named_args = parser.add_argument_group('optional arguments')
    opt_named_args.add_argument("--debug", required=False)
    opt_named_args.add_argument("--pretrain", required=False)
    opt_named_args.add_argument("--state", required=False)
    opt_named_args.add_argument("--testing", required=False)


    args = vars(parser.parse_args())

    # Cast the arguments into the required types
    args["lr"]         = float(args["lr"])
    args["momentum"]   = float(args["momentum"])
    args["num_hidden"] = int(args["num_hidden"])
    args["batch_size"] = int(args["batch_size"])
    args["epochs"]     = int(args["epochs"])
    args["anneal"]     = (args["anneal"] == "True" or args["anneal"] == "true")
    args["sizes"]      = [int(size) for size in args["sizes"].split(",")]
    args["pretrain"]   = (args["pretrain"] == "True" or args["pretrain"] == "true")
    args["state"]      = 0 if args["state"] is None else int(args["state"])
    args["testing"]    = (args["testing"] == "True" or args["testing"] == "true")
    args["debug"]      = (args["debug"] != "false")

    return args



def main():
    gparams = parse_args()
    split_ds = read_split_dataset(**gparams)
    split_ds = standardize_dataset(split_ds)

    net = NeuralNet(**gparams)
    Optimizer = get_optimizer(gparams['opt'])
    optimizer = Optimizer(net, split_ds, **gparams)

    if not gparams["testing"]:
        optimizer.run(gparams['batch_size'], gparams['epochs'], gparams['state'])
    J_train = compute_loss(net, split_ds.train, gparams['loss'])
    J_valid = compute_loss(net, split_ds.valid, gparams['loss'])
    J_train_accuracy = compute_accuracy(net, split_ds.train)
    J_valid_accuracy = compute_accuracy(net, split_ds.valid)
    print("Loss at the end: Train = {}; Valid = {};\n"\
          .format(J_train, J_valid))
    print("Accuracy at the end: Train = {}; Valid = {};\n"\
          .format(J_train_accuracy, J_valid_accuracy))
    Yhat = net.forward(split_ds.test.X)
    write_kaggle_submission("predictions_" + str(gparams["state"]), Yhat)


if __name__ == '__main__':
    main()

