#!/usr/bin/env bash

python train.py --lr 0.001 --momentum 0.5 --num_hidden 2 \
                 --sizes 300,100 --activation relu \
                 --loss ce --opt adam --batch_size 128 \
                 --epochs 10 --anneal false --save_dir pa1/ \
                 --expt_dir pa1/exp1/ --train data/train_trunc.csv \
                 --val data/valid_trunc.csv --test data/test_trunc.csv \
                 --debug false --pretrain false --state 5 --testing false
