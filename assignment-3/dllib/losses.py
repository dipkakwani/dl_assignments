# -*- coding: utf-8 -*-
"""
Copyright © Divyanshu Kakwani, Diptanshu Kakwani, 2019, all rights reserved.

This file defines all the loss functions
"""

import unittest
import numpy as np
from abc import ABC, abstractmethod


all_losses = {}

def get_loss(name):
    if name not in all_losses:
        raise Exception('Loss not defined')
    return all_losses[name]


class Loss(ABC):
    """
    Abstract class for loss function. All losses
    must be derived from this class and, at the least,
    override the abstract methods

    Notes:
    --------
    * Layout of Y and Yhat: Each row of Y and Yhat
      corresponds to a specific y value
    """
    def __init_subclass__(cls, default_name, **kwargs):
        """
        This method is called whenever a subclass gets
        created
        """
        global all_losses
        all_losses[default_name] = cls

    @staticmethod
    @abstractmethod
    def compute(Y, Yhat):
        """
        @param Y A 1-d/2-d array holding the ground-truth values
        @param Yhat The predicted values. Must have the same dimension
                    as Y
        @return A scalar quantity which is the final loss value

        """
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def gradient(Y, Yhat):
        """
        Note that this does not return ∂L/∂yhat. Rather, it returns
        ∂L/∂a^{o}, which is the gradient w.r.t. the pre-activation of
        the final layer.

        @param Y A 1-d/2-d array holding the ground-truth values
        @param Yhat The predicted values. Must have the same dimension
                    as Y
        @return A scalar quantity which is the final loss value
        """
        raise NotImplementedError


class CrossEntropy(Loss, default_name="ce"):
    """
    Used for classification problems. Measures the
    similarity of two distributions
    Why use it?
    """
    @staticmethod
    def compute(Y, Yhat):
        sz = Y.shape[0]
        # To make sure we are not taking log of 0
        Yhat = np.clip(Yhat, 1e-12, None)
        return -np.sum(np.multiply(Y, np.log(Yhat)))/sz

    @staticmethod
    def gradient(Y, Yhat):
        return Yhat - Y


class MSE(Loss, default_name="mse"):
    """
    Generally used in the case of regression problems
    """
    @staticmethod
    def compute(Y, Yhat):
        divisor = np.prod(Y.shape)
        return np.sum(np.square(Yhat - Y)) / divisor

    @staticmethod
    def gradient(Y, Yhat):
        raise NotImplementedError


class TestLosses(unittest.TestCase):
    """
    Contains test cases for all the activation function.
    There is one method for each activation.
    """
    def test_all(self):
        pass


if __name__ == '__main__':
    unittest.main()
