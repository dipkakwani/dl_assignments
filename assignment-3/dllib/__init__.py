
from .losses import get_loss

def compute_loss(net, dataset, loss_name):
    """
    Computes loss over a dataset
    """
    loss = get_loss(name)
    Yhat = net.forward(dataset.X)
    J = loss.compute(dataset.Y, Yhat)
    return J

def compute_accuracy(net, dataset):
    """
    Computes accuracy over a dataset. Accuracy
    is defined as the percentage of correct
    classifications.
    """
    Yhat = net.forward(dataset.X)
    correct = 0
    num_rows = Yhat.shape[0]
    for i in range(num_rows):
        correct += (np.argmax(Yhat[i, :]) == np.argmax(dataset.Y[i, :]))
    return (correct/num_rows) * 100
