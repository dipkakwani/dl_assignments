
import numpy as np

def softmax(X):
    """
    @param X a 1-D/2-D array
    @return An array having the same dimension as X
    """
    # for numerical stability (?), we subract from the max element
    numerator = np.exp(X - np.max(X, axis=0))
    denominator = numerator.sum(axis=0)
    return numerator/denominator


def transpose_vec(vec):
    """
    Transposes the vector `vec`. `vec` can be either
    2-d (a column vector) or 1-d (a row vector)

    @param vec A 1-d or 2-d array
    @return Transposed vector
    """
    # Must be a 1-d array or a 2-d array with only one column
    assert vec.ndim == 1 or vec.shape[1] == 1

    if vec.ndim == 1:
        return vec[np.newaxis].T
    else:
        return vec.T[0]


def elemwise_add(iterable1, iterable2):
    """
    Adds two iterables

    Note that to handle None, we define:
        None + a = a
    """
    result = []

    if iterable1 is None or iterable2 is None:
        return iterable1 or iterable2

    size = len(iterable1)
    for i in range(size):
        if iterable1[i] is None or iterable2[i] is None:
            result.append(iterable1[i] or iterable2[i])
        else:
            result.append(iterable1[i] + iterable2[i])
    return result


def write_kaggle_submission(filename, Yhat):
    """
    Generates a csv file that can be submitted on Kaggle
    Yhat representation: each prediction is stored in a row
    """
    Ycol = np.apply_along_axis(lambda a: np.argmax(a), 1, Yhat)
    # Ycol becomes a 1-D array
    Ycol = transpose_vec(Ycol)
    id_col = np.arange(0, Yhat.shape[0])
    id_col = transpose_vec(id_col)
    A = np.hstack((id_col, Ycol))
    A = A.astype(int)
    np.savetxt(filename, A, delimiter=",", fmt='%i', header="id,label", comments='')


def write_images(dataset, dir, num_images=50):
    """
    Writes some of the images in the dataset in
    the directory `dir`. This can be used for
    manual inspection of the dataset
    """
    it = DatasetIterator(dataset)
    for k in range(num_images):
        x = dataset.get(k)
        arr = x.reshape(28, 28)
        im = Image.fromarray(arr, 'L')
        write_path = os.path.join(dir, '{}.jpg'.format(k))
        im.save(write_path)
    return
