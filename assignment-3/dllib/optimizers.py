
import unittest
import numpy as np
from abc import ABC, abstractmethod
from .dataops import DatasetIterator
from .losses import get_loss
from .utils import elemwise_add
from .logger import Logger

all_optims = {}

def get_optimizer(name):
    if name not in all_optims:
        raise Exception('Optimizer not defined')
    return all_optims[name]


class Optimizer(ABC):
    """
    The base class for an optimizer
    """
    def __init_subclass__(cls, default_name, **kwargs):
        """
        This method is called whenever a subclass gets
        created
        """
        global all_losses
        all_optims[default_name] = cls

    def __init__(self, net, split_ds, **kwargs):
        self.net      = net
        self.train_ds = split_ds.train
        self.valid_ds = split_ds.valid
        self.loss     = get_loss(kwargs["loss"])
        self.lr       = kwargs["lr"]
        self.anneal   = kwargs["anneal"]
        self.debug    = kwargs["debug"]
        self.log      = Logger(kwargs["expt_dir"] + "log_train.txt")

    def _get_grads(self, batch):
        """
        Computes the gradients for the input batch
        by invoking forward and backward on self.net
        """
        Yhat = self.net.forward(batch.X)
        grad_Ao = self.loss.gradient(batch.Y, Yhat)
        self.net.backward(grad_Ao)
        return (self.net.grad_W, self.net.grad_b)

    def run(self, batch_size, max_epochs, start_epoch=0):
        """
        Kicks off the training
        """
        dataset_it = DatasetIterator(self.train_ds)
        J_train_prev, J_valid_prev = float("inf"), float("inf")
        J_valid_best, best_net = float("inf"), None

        for epoch_id in range(start_epoch, max_epochs):
            step_no = 0
            dataset_it.shuffle()

            for batch in dataset_it.batches(batch_size):
                step_no += 1
                grad_W, grad_b = self._get_grads(batch)
                dW, db = self.compute_delta(batch, grad_W, grad_b, step_no)
                self.net.W = elemwise_add(self.net.W, dW)
                self.net.b = elemwise_add(self.net.b, db)

            J_train = self._compute_loss(self.train_ds)
            J_valid = self._compute_loss(self.valid_ds)
            J_train_accuracy = self._compute_accuracy(self.train_ds)
            J_valid_accuracy = self._compute_accuracy(self.valid_ds)
            print("Loss at the end of epoch no. {}: Train = {}; Valid = {};"\
                  .format(epoch_id, J_train, J_valid))
            print("Accuracy at the end of epoch no. {}: Train = {}; Valid = {};\n"\
                  .format(epoch_id, J_train_accuracy, J_valid_accuracy))

            if self.debug:
                if step_no % 100 == 0:
                    self.log.write(epoch=epoch_id, step=step_no, loss=J_train,\
                                    error=(100 - J_train_accuracy), lr=self.lr)
                # Dump the current parameters to file
                self.net.save_params(epoch_id)

            # Check if we should halven the learning rate
            if self.anneal and J_valid_prev < J_valid:
                self.lr /= 2

            if J_valid <= J_valid_best:
                J_valid_best = J_valid
                best_net = self.net.deepcopy()
            J_train_prev = J_train
            J_valid_prev = J_valid

        # If there was a better loss during the training
        if (J_valid_prev > J_valid_best):
            self.net.W = best_net.W
            self.net.b = best_net.b
        self.log.close()

    @abstractmethod
    def compute_delta(self, batch, grad_W, grad_b, num_steps):
        raise NotImplementedError


class SGDOptimizer(Optimizer, default_name="sgd"):

    def __init__(self, net, split_ds, **kwargs):
        super().__init__(net, split_ds, **kwargs)

    def compute_delta(self, batch, grad_W, grad_b, step_no):
        dW = [None] * self.net.depth
        db = [None] * self.net.depth
        for k in range(1, self.net.depth):
            dW[k] = -self.lr * grad_W[k]
            db[k] = -self.lr * grad_b[k]
        return dW, db


class MomentumOptimizer(Optimizer, default_name="momentum"):

    def __init__(self, net, split_ds, **kwargs):
        super().__init__(net, split_ds, **kwargs)
        self.mmt = kwargs["momentum"]
        self.depth = self.net.depth
        self.dW = [np.zeros_like(self.net.W[i]) for i in range(self.depth)]
        self.db = [np.zeros_like(self.net.b[i]) for i in range(self.depth)]

    def compute_delta(self, batch, grad_W, grad_b, step_no):
        for k in range(1, self.depth):
            self.dW[k] = self.mmt * self.dW[k] - self.lr * grad_W[k]
            self.db[k] = self.mmt * self.db[k] - self.lr * grad_b[k]
        return self.dW, self.db


class NAGOptimizer(Optimizer, default_name="nag"):

    def __init__(self, net, split_ds, **kwargs):
        super().__init__(net, split_ds, **kwargs)
        self.mmt = kwargs["momentum"]
        self.depth = self.net.depth
        self.dW = [np.zeros_like(self.net.W[i]) for i in range(self.depth)]
        self.db = [np.zeros_like(self.net.b[i]) for i in range(self.depth)]
        self.net_copy = self.net.deepcopy()

    def compute_delta(self, batch, grad_W, grad_b, step_no):
        for k in range(1, self.depth):
            self.net_copy.W[k] = self.net_copy.W[k] - self.mmt * self.dW[k]
            self.net_copy.b[k] = self.net_copy.b[k] - self.mmt * self.db[k]

        ahead_dW, ahead_dB = self.net_copy._get_grads(batch)

        for k in range(1, self.depth):
            self.dW[k] = -self.mmt * self.dW[k] - self.lr * ahead_dW[k]
            self.db[k] = -self.mmt * self.db[k] - self.lr * ahead_dB[k]

        return self.dW, self.db


class AdamOptimizer(Optimizer, default_name="adam"):

    def __init__(self, net, split_ds, **kwargs):
        super().__init__(net, split_ds, **kwargs)
        self.t         = 1
        self.beta1     = 0.9
        self.beta2     = 0.999
        self.eps       = 0.0001
        self.m_t_W     = []
        self.v_t_W     = []
        self.m_t_hat_W = []
        self.v_t_hat_W = []
        self.m_t_b     = []
        self.v_t_b     = []
        self.m_t_hat_b = []
        self.v_t_hat_b = []
        self.dW        = []
        self.db        = []
        self.depth     = self.net.depth

        for k in range(self.depth):
            self.m_t_W.append(np.zeros_like(self.net.W[k]))
            self.v_t_W.append(np.zeros_like(self.net.W[k]))
            self.m_t_hat_W.append(np.zeros_like(self.net.W[k]))
            self.v_t_hat_W.append(np.zeros_like(self.net.W[k]))
            self.m_t_b.append(np.zeros_like(self.net.b[k]))
            self.v_t_b.append(np.zeros_like(self.net.b[k]))
            self.m_t_hat_b.append(np.zeros_like(self.net.b[k]))
            self.v_t_hat_b.append(np.zeros_like(self.net.b[k]))
            self.dW.append(np.zeros_like(self.net.W[k]))
            self.db.append(np.zeros_like(self.net.b[k]))

    def compute_delta(self, batch, grad_W, grad_b, step_no):
        for k in range(1, self.depth):
            self.m_t_W[k] = self.beta1 * self.m_t_W[k] + (1 - self.beta1) * grad_W[k]
            self.v_t_W[k] = self.beta2 * self.v_t_W[k] + (1 - self.beta2) * (grad_W[k] ** 2)
            self.m_t_b[k] = self.beta1 * self.m_t_b[k] + (1 - self.beta1) * grad_b[k]
            self.v_t_b[k] = self.beta2 * self.v_t_b[k] + (1 - self.beta2) * (grad_b[k] ** 2)
            self.m_t_hat_W[k] = self.m_t_W[k] / (1 - self.beta1 ** self.t)
            self.v_t_hat_W[k] = self.v_t_W[k] / (1 - self.beta2 ** self.t)
            self.m_t_hat_b[k] = self.m_t_b[k] / (1 - self.beta1 ** self.t)
            self.v_t_hat_b[k] = self.v_t_b[k] / (1 - self.beta2 ** self.t)

        for k in range(1, self.depth):
            self.dW[k] = -self.lr * self.m_t_hat_W[k] / (np.sqrt(self.v_t_hat_W[k]) + self.eps)
            self.db[k] = -self.lr * self.m_t_hat_b[k] / (np.sqrt(self.v_t_hat_b[k]) + self.eps)

        self.t += 1
        return self.dW, self.db

