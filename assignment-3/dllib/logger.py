import os

class Logger:

    def __init__(self, path):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        self.ostream = open(path, 'w')

    def close(self):
        self.ostream.close()

    def write(self, **kwargs):
        line_tpl = 'Epoch {}, Step {}, Loss: {}, Error: {}, lr: {}\n'
        line = line_tpl.format(kwargs['epoch'], kwargs['step'],
                               kwargs['loss'], kwargs['error'],
                               kwargs['lr'])
        self.ostream.write(line)

