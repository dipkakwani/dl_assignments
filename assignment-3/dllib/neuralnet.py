# -*- coding: utf-8 -*-
"""
Copyright © Divyanshu Kakwani, Diptanshu Kakwani, 2019, all rights reserved.

This file contains an implementation of MLFNN
"""

import math
import numpy as np
import pickle
import copy
from .activations import get_activation
from .utils import softmax, transpose_vec


class NeuralNet:
    """
    Fully connected multi-layer feed forward neural network
    """
    def __init__(self, **params):
        self.dtype      = np.float64
        self.in_sz      = 784
        self.out_sz     = 10
        self.num_units  = [self.in_sz] + params["sizes"] + [self.out_sz]
        self.depth      = len(self.num_units)
        self.activ      = get_activation(params["activation"])
        self.save_dir   = params["save_dir"]

        # Network internal values
        self.H = [None] * self.depth
        self.A = [None] * self.depth

        # Gradient variables
        self.grad_W = [None] * self.depth
        self.grad_b = [None] * self.depth
        self.grad_A = [None] * self.depth
        self.grad_H = [None] * self.depth

        if params["pretrain"] == True:
            self.load_params(params["state"])
        else:
            self.init_params()

    def deepcopy(self):
        """
        Creates a copy of the entire network. This method
        can be used by optimizers like nag.
        """
        return copy.deepcopy(self)

    def init_params(self, method='He'):
        """
        Initializes the network parameters W and b
        """
        # Network parameters
        self.W = [None] * self.depth
        self.b = [None] * self.depth

        np.random.seed(1234)

        for i in range(1, self.depth):
            n1 = self.num_units[i-1]
            n2 = self.num_units[i]
            if method == 'He':
                rands = np.random.normal(0, math.sqrt(2/n2), n2*n1)
            elif method == 'random':
                rands = np.random.normal(0, 0.01, n2*n1)
            self.W[i] = rands.reshape(n2, n1)
            self.b[i] = np.zeros((n2, 1))
        return

    def save_params(self, epoch):
        """
        Dump neural network parameters to a file.
        :param epoch: The current epoch number.
        :return:
        """
        with open(self.save_dir + 'weights_{}.pkl'.format(epoch), 'wb') as f:
            pickle.dump(self.W, f)
            pickle.dump(self.b, f)

    def load_params(self, epoch):
        """
        Load the parameters from the file.
        :param epoch: The current epoch number.
        :return:
        """
        with open(self.save_dir + 'weights_{}.pkl'.format(epoch), 'rb') as f:
            self.W = pickle.load(f)
            self.b = pickle.load(f)

    def forward(self, X):
        """
        Forward propagates X through the network

        @param X is a 1-d or 2-d array. In case of 2-d array,
                 it is assumed that each row represents an
                 input instance x
        @return the output of the network. In case of 1-d input
                it returns a 1-d output. In case of 2-d input,
                it returns an array where the ith row represents
                the output of the ith input
        """
        if X.ndim == 1:
            self.H[0] == transpose_vec(X)
        else:
            self.H[0] = X.T

        # Compute A's and H's iteratively for each layer
        for k in range(1, self.depth):
            self.A[k] = self.b[k] + np.matmul(self.W[k], self.H[k-1])
            self.H[k] = self.activ.apply(self.A[k])

        # Apply softmax on the pre-activation of the last layer
        self.H[self.depth-1] = softmax(self.A[self.depth-1])

        if X.ndim == 1:
            return transpose_vec(self.H[self.depth-1])
        return self.H[self.depth-1].T

    def backward(self, grad_Ao, reg_coeff=0.05):
        """
        Backward propagates the final layer's gradient grad_Ao

        @param grad_Ao is a 1-d or 2-d array
        """
        if grad_Ao.ndim == 1:
            grad_Ao = transpose_vec(grad_Ao)
        else:
            grad_Ao = grad_Ao.T

        batch_size = grad_Ao.shape[1]

        for k in range(self.depth-1, 0, -1):
            if k != self.depth-1:
                self.grad_A[k] = np.multiply(self.grad_H[k],
                                             self.activ.gradient(self.A[k]))
            else:
                self.grad_A[k] = grad_Ao
            self.grad_b[k] = transpose_vec(np.sum(self.grad_A[k], axis=1))
            self.grad_W[k] = np.matmul(self.grad_A[k], self.H[k-1].T) \
                             + reg_coeff * self.W[k]    # regularization term
            # Note that the regularization term is only an approximation;
            self.grad_H[k-1] = np.matmul(self.W[k].T, self.grad_A[k])
            # grad_b, grad_w store sums; divide by batch_size to get the
            # average
            self.grad_b[k] /= batch_size
            self.grad_W[k] /= batch_size
        return

