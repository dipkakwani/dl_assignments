# -*- coding: utf-8 -*-
"""
Copyright © Divyanshu Kakwani, Diptanshu Kakwani, 2019, all rights reserved.

This file defines all the activations functions
"""

import unittest
import numpy as np
from abc import ABC, abstractmethod


all_activations = {}

def get_activation(name):
    if name not in all_activations:
        raise Exception('Activation not defined')
    return all_activations[name]


class Activation(ABC):
    """
    Abstract class for activations. All activations
    must be derived from this class and, at the least,
    override the abstract methods
    """
    def __init_subclass__(cls, default_name, **kwargs):
        """
        This method is called whenever a new subclass
        is created
        """
        global all_activations
        all_activations[default_name] = cls

    @staticmethod
    @abstractmethod
    def apply(X):
        """
        @param X Input to the activation. It can be a 2-D, 1-D
                 or 0-D (just a scalar) array
        @return Output of the activation function. It has the
                same dimension as the input.
        """
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def gradient(X):
        """
        @param X Input to the activation. It can be a 2-D, 1-D
                 or 0-D (just a scalar) array
        @return Output of the activation function. It has the
                same dimension as the input.
        """
        raise NotImplementedError


class Sigmoid(Activation, default_name="sigmoid"):
    """
    Compresses the input to [0, 1]
    Why use it?
    """
    @staticmethod
    def apply(X):
        return 1 / (1 + np.exp(-X))

    @staticmethod
    def gradient(X):
        return np.multiply(sigmoid(X), (1 - sigmoid(X)))


class ReLu(Activation, default_name="relu"):
    """
    This activation function converges faster
    """
    @staticmethod
    def apply(X):
        return np.maximum(X, 0)

    @staticmethod
    def gradient(X):
        return (X > 0) * 1


class Tanh(Activation, default_name="tanh"):
    """
    """
    @staticmethod
    def apply(X):
        return np.tanh(X)

    @staticmethod
    def gradient(X):
        return 1 - np.tanh(X) ** 2


class TestActivations(unittest.TestCase):
    """
    Contains test cases for all the activation function.
    There is one method for each activation.
    """
    def test_all(self):
        pass


if __name__ == '__main__':
    unittest.main()
