"""
The dataset is represented as numpy arrays
throughout the program. For convenience, this
file defines some tuple types that stitch
together multiple variables

"""

import numpy as np
from collections import namedtuple
from .utils import transpose_vec


# Types for stitching multiple variables
SplitDataset      = namedtuple('SplitDataset', ['train', 'valid', 'test'])
LabelledDataset   = namedtuple('LabelledDataset', ['X', 'Y'])
UnlabelledDataset = namedtuple('UnlabelledDataset', ['X'])


def standardize_dataset(split_ds):
    """
    Standardizes the dataset referenced by split_ds
    """
    n1 = split_ds.train.X.shape[0]
    n2 = split_ds.valid.X.shape[0]
    n3 = split_ds.test.X.shape[0]

    m1 = np.mean(split_ds.train.X, axis=0)
    m2 = np.mean(split_ds.valid.X, axis=0)
    m3 = np.mean(split_ds.test.X, axis=0)
    mean = (m1 * n1 + m2 * n2 + m3 * n3) / (n1 + n2 + n3)

    s1 = np.std(split_ds.train.X, axis=0)
    s2 = np.std(split_ds.valid.X, axis=0)
    s3 = np.std(split_ds.test.X, axis=0)
    deviance = (n1*(s1**2) + n2*(s2**2) + n3*(s3**2) + \
                n1*(m1-mean)**2 + n2*(m2-mean)**2 + n3*(m3-mean)**2)
    stddev = np.sqrt(deviance/(n1+n2+n3))

    X1 = (split_ds.train.X - mean) / stddev
    X2 = (split_ds.valid.X - mean) / stddev
    X3 = (split_ds.test.X - mean) / stddev

    return SplitDataset(LabelledDataset(X1, split_ds.train.Y),
                        LabelledDataset(X2, split_ds.valid.Y),
                        UnlabelledDataset(X3))


def read_split_dataset(**kwargs):
    ds1 = read_dataset(kwargs["train"])
    ds2 = read_dataset(kwargs["val"])
    ds3 = read_dataset(kwargs["test"], labelled=False)
    return SplitDataset(ds1, ds2, ds3)


def read_dataset(path, labelled=True, dtype=np.float64):
    """
    Reads an unlabelled or labelled dataset from a file
    @return An array X, and also Y if the dataset is labelled
    """
    X = np.loadtxt(path, dtype=dtype, delimiter=',', skiprows=1)
    if labelled:
        Y = X[:, -1].astype(np.uint8)
        Y = np.eye(10)[Y]
        X = X[:, 1:-1]
        return LabelledDataset(X, Y)
    else:
        X = X[:, 1:]
        return UnlabelledDataset(X)


def write_dataset(filename, X, Y):
    """
    Writes the labelled dataset X, Y in the
    csv file `filename` in the same format
    as that of other csv files
    """
    Ycol = np.apply_along_axis(lambda a: np.argmax(a), 1, Y)
    Ycol = transpose_vec(Ycol)
    id_col = np.arange(0, X.shape[0])
    id_col = transpose_vec(id_col)
    entire_arr = np.hstack((id_col, X, Ycol))
    entire_arr = entire_arr.astype(int)
    np.savetxt(filename, entire_arr, delimiter=",", fmt='%i', header="id,label")


class DatasetIterator:
    """
    This is a light weight iterator class for
    iterating over a labelled dataset. It supports
    two methods of iteration - instance-wise, and
    batch-wise.
    """
    def __init__(self, labelled_ds):
        self.X = labelled_ds.X
        self.Y = labelled_ds.Y
        self.size = self.X.shape[0]

    def get(self, index):
        return self.X[index], self.Y[index]

    def get_batch(self, index, batch_size=32):
        start = batch_size * index
        end = batch_size * (index + 1)
        subX = self.X[start:end, :].view()
        subY = self.Y[start:end, :].view()
        return LabelledDataset(subX, subY)

    def instances(self):
        for index in range(self.size):
            yield self.get(index)

    def batches(self, batch_sz=32):
        num_batches = int(self.size / batch_sz)
        for idx in range(num_batches):
            yield self.get_batch(idx, batch_sz)

    def shuffle(self):
        self.X, self.Y = shuffle(self.X, self.Y)


def shuffle(X, Y):
    p = np.random.permutation(len(X))
    return X[p], Y[p]
