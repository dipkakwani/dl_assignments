#!/usr/bin/env bash

python train.py --lr 0.001 --momentum 0.5 --num_hidden 2 \
                 --sizes 600,300 --activation relu \
                 --loss ce --opt adam --batch_size 128 \
                 --epochs 50 --anneal false --save_dir pa1/ \
                 --expt_dir pa1/exp1/ --train data/train+valid.csv \
                 --val data/valid.csv --test data/test.csv \
                 --debug false --pretrain false --state 0 --testing false
