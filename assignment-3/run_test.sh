#!/usr/bin/env bash

python test.py --lr 0.001 --momentum 0.5 --num_hidden 2 \
                 --sizes 300,100 --activation relu \
                 --loss ce --opt adam --batch_size 128 \
                 --epochs 50 --anneal false --save_dir pa1/ \
                 --expt_dir pa1/exp1/ --train data/train.csv \
                 --val data/valid.csv --test data/test_trunc.csv \
                 --debug false --pretrain false --state 5 --testing false
