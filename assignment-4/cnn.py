"""
Implementation of CNN
TODO:
Use TF saver to save model
Implement early stopping
Use augmented data
"""
import tensorflow as tf
import os
import numpy as np
from tensorflow.python.ops import nn_ops, gen_nn_ops
from tensorflow.python.framework import ops
import random

class CNN:

    def __init__(self, **params):
        self.n_classes = 20
        self.params = params
        self.initializer = tf.contrib.layers.xavier_initializer() if params["init"] == 1 \
                            else tf.contrib.layers.variance_scaling_initializer()
        #self.regularizer = tf.contrib.layers.l2_regularizer(scale=0.01)
        self.save_path = None


    def create_network(self, X, is_training):
        conv1 = tf.layers.conv2d(inputs=X,
                                 filters=96,
                                 kernel_size=[3, 3],
                                 strides=(1, 1),
                                 padding='same',
                                 #activation=tf.nn.relu,
                                 use_bias=True,
                                 kernel_initializer=self.initializer,
                                 bias_initializer=self.initializer,
                                 kernel_regularizer=None,
                                 bias_regularizer=None,
                                 activity_regularizer=None,
                                 name="conv1")

        conv1 = tf.layers.batch_normalization(conv1, training=is_training)
        conv1 = tf.nn.relu(conv1)
        #conv1 = tf.layers.dropout(inputs=conv1, rate=0.8, training=is_training)

        conv2 = tf.layers.conv2d(inputs=conv1,
                                 filters=96,
                                 kernel_size=[3, 3],
                                 strides=(1, 1),
                                 padding='same',
                                 #activation=tf.nn.relu,
                                 use_bias=True,
                                 kernel_initializer=self.initializer,
                                 bias_initializer=self.initializer,
                                 kernel_regularizer=None,
                                 bias_regularizer=None,
                                 activity_regularizer=None,
                                 name="conv2")

        conv2 = tf.layers.batch_normalization(conv2, training=is_training)
        conv2 = tf.nn.relu(conv2)
        #conv2 = tf.layers.dropout(inputs=conv2, rate=0.8, training=is_training)

        pool1 = tf.layers.max_pooling2d(inputs=conv2,
                                        pool_size=[2, 2],
                                        strides=2,
                                        padding='same')

        conv3 = tf.layers.conv2d(inputs=pool1,
                                 filters=256,
                                 kernel_size=[3, 3],
                                 strides=(1, 1),
                                 padding='same',
                                 #activation=tf.nn.relu,
                                 use_bias=True,
                                 kernel_initializer=self.initializer,
                                 bias_initializer=self.initializer,
                                 kernel_regularizer=None,
                                 bias_regularizer=None,
                                 activity_regularizer=None,
                                 name="conv3")

        conv3 = tf.layers.batch_normalization(conv3, training=is_training)
        conv3 = tf.nn.relu(conv3)
        #conv3 = tf.layers.dropout(inputs=conv3, rate=0.8, training=is_training)

        conv4 = tf.layers.conv2d(inputs=conv3,
                                 filters=256,
                                 kernel_size=[3,3],
                                 strides=(1, 1),
                                 padding='same',
                                 #activation=tf.nn.relu,
                                 use_bias=True,
                                 kernel_initializer=self.initializer,
                                 bias_initializer=self.initializer,
                                 kernel_regularizer=None,
                                 bias_regularizer=None,
                                 activity_regularizer=None,
                                 name="conv4")

        conv4 = tf.layers.batch_normalization(conv4, training=is_training)
        conv4 = tf.nn.relu(conv4)
        #conv4 = tf.layers.dropout(inputs=conv4, rate=0.8, training=is_training)

        pool2 = tf.layers.max_pooling2d(inputs=conv4,
                                        pool_size=[2, 2],
                                        strides=2,
                                        padding='same')

        conv5 = tf.layers.conv2d(inputs=pool2,
                                 filters=512,
                                 kernel_size=[3, 3],
                                 strides=(1, 1),
                                 padding='same',
                                 #activation=tf.nn.relu,
                                 use_bias=True,
                                 kernel_initializer=self.initializer,
                                 bias_initializer=self.initializer,
                                 kernel_regularizer=None,
                                 bias_regularizer=None,
                                 activity_regularizer=None,
                                 name="conv5")

        conv5 = tf.layers.batch_normalization(conv5, training=is_training)
        conv5 = tf.nn.relu(conv5)
        #conv5 = tf.layers.dropout(inputs=conv5, rate=0.8, training=is_training)

        conv6 = tf.layers.conv2d(inputs=conv5,
                                 filters=512,
                                 kernel_size=[3, 3],
                                 strides=(1, 1),
                                 padding='valid',
                                 #activation=tf.nn.relu,
                                 use_bias=True,
                                 kernel_initializer=self.initializer,
                                 bias_initializer=self.initializer,
                                 kernel_regularizer=None,
                                 bias_regularizer=None,
                                 activity_regularizer=None,
                                 name="conv6")

        conv6 = tf.layers.batch_normalization(conv6, training=is_training)
        conv6 = tf.nn.relu(conv6)
        #conv6 = tf.layers.dropout(inputs=conv6, rate=0.8, training=is_training)

        pool3 = tf.layers.max_pooling2d(inputs=conv6,
                                        pool_size=[2, 2],
                                        strides=2,
                                        padding='same')

        flat_layer = tf.reshape(pool3, [-1, 7 * 7 * 512])

        fc1 = tf.layers.dense(inputs=flat_layer,
                              units=1024,
                             #activation=tf.nn.relu,
                              use_bias=True,
                              kernel_initializer=self.initializer,
                              bias_initializer=self.initializer,
                              kernel_regularizer=None,
                              bias_regularizer=None,
                              activity_regularizer=None,
                              name="fc1")

        fc1norm = tf.layers.batch_normalization(fc1, training=is_training)
        fc1norm = tf.nn.relu(fc1norm)
        #fc1_dropout = tf.layers.dropout(inputs=fc1norm, rate=0.8, training=is_training)

        fc2 = tf.layers.dense(inputs=fc1norm,
                              units=256,
                              # activation=tf.nn.relu,
                              use_bias=True,
                              kernel_initializer=self.initializer,
                              bias_initializer=self.initializer,
                              kernel_regularizer=None,
                              bias_regularizer=None,
                              activity_regularizer=None,
                              name="fc2")

        fc2norm = tf.layers.batch_normalization(fc2, training=is_training)
        fc2norm = tf.nn.relu(fc2norm)
        #fc2_dropout = tf.layers.dropout(inputs=fc2norm, rate=0.8, training=is_training)

        logits = tf.layers.dense(inputs=fc2norm,
                                 units=self.n_classes,
                                 name="fc3")
        
        #logits = tf.layers.batch_normalization(logits, training=is_training)
        #self.visualize_kernel()

        tf.add_to_collection('BP', X)
        tf.add_to_collection('BP', is_training)
        tf.add_to_collection('BP', conv1)
        tf.add_to_collection('BP', conv2)
        tf.add_to_collection('BP', conv3)
        tf.add_to_collection('BP', conv4)
        tf.add_to_collection('BP', conv5)
        tf.add_to_collection('BP', conv6)

        return logits


    def train(self, train_X, train_Y, val_X, val_Y, test_X, class1):

        x = tf.placeholder("float", [None, 64, 64, 3], name="X")
        y = tf.placeholder("float", [None, self.n_classes])
        is_training = tf.placeholder("bool", name="is_training")


        pred = self.create_network(x, is_training)

        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=pred, labels=y))
               #tf.losses.get_regularization_loss()

        #merged = self.visualize_kernel()
        sample_images = tf.placeholder("float", [None, 64, 64, 3], name="sample_images")

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

        with tf.control_dependencies(update_ops):
            optimizer = tf.train.AdamOptimizer(learning_rate=self.params["lr"]).minimize(cost)
            #optimizer = tf.train.RMSPropOptimizer(learning_rate=self.params["lr"]).minimize(cost)

        correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float64))

        init = tf.global_variables_initializer()
        batch_size = self.params["batch_size"]
        saver = tf.train.Saver()
        best_loss = 1 << 20
        patience = 5

        sess = tf.Session()
        with sess.as_default():
            sess.run(init)
            train_loss = []
            test_loss = []
            train_accuracy = []
            test_accuracy = []
            summary_writer = tf.summary.FileWriter('./Output', sess.graph)

            for i in range(self.params["epochs"]):

                for batch in range(len(train_X) // batch_size):
                    batch_x = train_X[batch * batch_size:min((batch + 1) * batch_size, len(train_X))]
                    batch_y = train_Y[batch * batch_size:min((batch + 1) * batch_size, len(train_Y))]

                    opt = sess.run(optimizer, feed_dict={x: batch_x,
                                                         y: batch_y,
                                                         is_training: True})
                    loss, acc = sess.run([cost, accuracy],
                                         feed_dict={x: batch_x,
                                                    y: batch_y,
                                                    is_training: True})

                print("Iter " + str(i) + ", Loss= " + \
                              "{:.6f}".format(loss) + ", Training Accuracy= " + \
                              "{:.5f}".format(acc * 100.0))


                test_acc,valid_loss = sess.run([accuracy,cost],
                                               feed_dict={x: val_X,
                                                          y : val_Y,
                                                          is_training: False})

                train_loss.append(loss)
                test_loss.append(valid_loss)
                train_accuracy.append(acc)
                test_accuracy.append(test_acc)

                print("Validation Loss= {:.5f} ".format(valid_loss) + \
                      "Accuracy= {:.5f}".format(test_acc * 100.0))

                if valid_loss < best_loss:
                    self.save_path = saver.save(sess,
                                                self.params["save_dir"] +
                                                "model-" + str(i) + ".ckpt")
                    patience = 5
                    best_loss = valid_loss
                else:
                    patience -= 1
                    if patience <= 0 and i >= 10:
                        break

                if i <= 10:     #Ignore val_loss less than 20 epochs
                    best_loss = valid_loss
                    patience = 5

            saver.restore(sess, self.save_path)
            test_Y = sess.run(pred, feed_dict={x: test_X,
                                                   is_training: False})

            self.write_to_file(test_Y)
            #summary = self.guided_backprop(train_X)
            #summary = sess.run(merged)
            #self.guided_backprop(class1)
            #summary_writer.add_summary(summary)
            summary_writer.close()
        sess.close()


    def write_to_file(self, Yhat):
        filepath = os.path.join(self.params["save_dir"], "predictions.txt")
        os.makedirs(self.params["save_dir"], exist_ok=True)
        Ycol = np.apply_along_axis(lambda a: np.argmax(a), 1, Yhat)
        # Ycol becomes a 1-D array
        Ycol = self.transpose_vec(Ycol)
        id_col = np.arange(0, Yhat.shape[0])
        id_col = self.transpose_vec(id_col)
        A = np.hstack((id_col, Ycol))
        A = A.astype(int)
        np.savetxt(filepath, A, delimiter=",", fmt='%i', header="id,label", comments='')

    def transpose_vec(self, vec):
        """
        Transposes the vector `vec`. `vec` can be either
        2-d (a column vector) or 1-d (a row vector)

        @param vec A 1-d or 2-d array
        @return Transposed vector
        """
        # Must be a 1-d array or a 2-d array with only one column
        assert vec.ndim == 1 or vec.shape[1] == 1

        if vec.ndim == 1:
            return vec[np.newaxis].T
        else:
            return vec.T[0]


    def visualize_kernel(self, g):
        #g = tf.get_default_graph()
        kernel = g.get_tensor_by_name('conv1/kernel:0')
        print(kernel.shape)

        x_min = tf.reduce_min(kernel)
        x_max = tf.reduce_max(kernel)
        kernel = (kernel - x_min) / (x_max - x_min)

        Y = kernel.shape[0] + 2
        X = kernel.shape[1] + 2
        grid_X = 8
        grid_Y = 8
        channels = 3

        x = tf.pad(kernel, tf.constant( [[1,1],[1, 1],[0,0],[0,0]] ), mode = 'CONSTANT', constant_values=1)
        # put NumKernels to the 1st dimension
        x = tf.transpose(x, (3, 0, 1, 2))
        # organize grid on Y axis
        x = tf.reshape(x, tf.stack([grid_X, Y * grid_Y, X, channels]))

        # switch X and Y axes
        x = tf.transpose(x, (0, 2, 1, 3))
        # organize grid on X axis
        x = tf.reshape(x, tf.stack([1, X * grid_X, Y * grid_Y, channels]))

        # back to normal order (not combining with the next step for clarity)
        x = tf.transpose(x, (2, 1, 3, 0))

        # to tf.image_summary order [batch_size, height, width, channels],
        #   where in this case batch_size == 1
        x = tf.transpose(x, (3, 0, 1, 2))

        print(x.shape)

        tf.summary.image('conv1/kernels', x, max_outputs=1)
        #return tf.summary.merge_all()

    def conv_output(self, images):
        conv1 = g.get_tensor_by_name('conv1') # FIXME wrong, op name
        sess = tf.get_default_session()
        out = sess.run(conv1, feed_dict={X : images})

        tf.summary.image('conv1', conv1, max_outputs=1)


    def guided_backprop(self, sample_images):


        with tf.Session() as sess:
            g = tf.get_default_graph()
            with g.gradient_override_map({'Relu': 'GuidedRelu'}):
                new_saver = tf.train.import_meta_graph(self.save_path + '.meta')

            new_saver.restore(sess, tf.train.latest_checkpoint(self.params["save_dir"]))

            summary_writer = tf.summary.FileWriter('./Output1', sess.graph)

            activations = tf.get_collection('BP')
            X = activations[0]
            conv6 = activations[7]
            is_training = activations[1]
            grads = [tf.gradients(conv6[:,:,:,i], X)[0] for i in range(10)]

            features = sess.run(grads[0], feed_dict={X: sample_images, is_training: False})
            print("Features shape", features.shape)
            print("Sample images shape", sample_images.shape)
            tf.summary.image('conv6', features, max_outputs=1)
            sample_image = tf.slice(sample_images, [1, 0, 0, 0], [1, 64, 64, 3]).eval()
            print("Sample image shape ", sample_image.shape)
            tf.summary.image('input_image', sample_image, max_outputs=1)

            self.visualize_kernel(g)
            merged = tf.summary.merge_all()
            summary = sess.run(merged)
            summary_writer.add_summary(summary)
            summary_writer.close()



@ops.RegisterGradient("GuidedRelu")
def _GuidedReluGrad(op, grad):
    return tf.where(0. < grad, gen_nn_ops._relu_grad(grad, op.outputs[0]), tf.zeros(tf.shape(grad)))
