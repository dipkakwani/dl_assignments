
import argparse
import numpy as np
import tensorflow as tf
from cnn import CNN
from data_tf import *


def parse_args():
    """
    Parses and processes the arguments obtained from the command
    lines and returns it as a dictionary.
    """
    parser = argparse.ArgumentParser()
    req_named_args = parser.add_argument_group("required named arguments")
    req_named_args.add_argument("--lr", required=True)
    req_named_args.add_argument("--batch_size", required=True)
    req_named_args.add_argument("--init", required=True)
    req_named_args.add_argument("--save_dir", required=True)
    req_named_args.add_argument("--epochs", required=True)
    req_named_args.add_argument("--dataAugment", required=True)
    req_named_args.add_argument("--train", required=True)
    req_named_args.add_argument("--val", required=True)
    req_named_args.add_argument("--test", required=True)

    args = vars(parser.parse_args())

    # Cast the arguments into appropriate types
    args["lr"]          = float(args["lr"])
    args["batch_size"]  = int(args["batch_size"])
    args["init"]        = int(args["init"])
    args["epochs"]      = int(args["epochs"])
    args["dataAugment"] = int(args["dataAugment"])

    return args


if __name__ == '__main__':
    gparams = parse_args()

    dataset = load_train(gparams["train"])
    train_X, train_Y = to_ndarr(dataset)
    mean, std = compute_stats(train_X)
    train_X = standardize(train_X, mean, std)

    val_dataset = load_val(gparams["val"])
    val_X, val_Y = to_ndarr(val_dataset)
    val_X = standardize(val_X, mean, std)

    test_dataset = load_test(gparams["test"])
    test_X = to_ndarr(test_dataset)
    test_X = standardize(test_X, mean, std)


    # class1 = load_train("data/class1.csv")
    # class1_X, class1_Y = to_ndarr(class1)
    # mean, std = compute_stats(class1_X)
    # class1_X = standardize(class1_X, mean, std)

    cnn = CNN(**gparams)
    cnn.train(train_X, train_Y, val_X, val_Y, test_X, None)
