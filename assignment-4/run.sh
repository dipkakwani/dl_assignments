#!/usr/bin/env bash

python3 main.py --lr 0.001 \
                 --batch_size 128 \
                 --epochs 40 --save_dir pa2/ \
                 --train data/train_trunc.csv \
                 --val data/valid_trunc.csv --test data/test_trunc.csv \
                 --init 2 --dataAugment 0
