"""
Copyright Divyanshu Kakwani, Diptanshu Kakwani 2019, all rights reserved.
"""

import random
import csv
import tensorflow as tf
import numpy as np

from PIL import Image


# For debugging purposes, enable this
#tf.enable_eager_execution()

# dataset-specific constants
nb_feats = 12288
img_shape = [64, 64, 3]


def load_train(path):
    """
    @return tf.data.Dataset
    """
    filenames = [path]
    record_defaults = [tf.float32] * (nb_feats + 2)
    dataset = tf.data.experimental.CsvDataset(filenames, record_defaults, header=True)
    map_fn = (lambda *row: {'id': row[0],
                            'features': tf.reshape(tf.stack(row[1:-1]), img_shape),
                            'label': tf.one_hot(tf.cast(row[-1], tf.int32), 20)})
    dataset = dataset.map(map_fn)
    return dataset


def load_val(path):
    """
    @return tf.data.Dataset
    """
    return load_train(path)

def load_test(path):
    """
    @return tf.data.Dataset
    """
    filenames = [path]
    record_defaults = [tf.float32] * (nb_feats + 1)
    dataset = tf.data.experimental.CsvDataset(filenames, record_defaults, header=True)
    # convert each row into three tensors - id, features, label
    map_fn = (lambda *row: {'id': row[0],
                            'features': tf.reshape(tf.stack(row[1:]), img_shape)})
    dataset = dataset.map(map_fn)
    return dataset


class Augmentors:
    """
    Defines several augmentors functions
    """
    @staticmethod
    def rotate(image):
        """
        This technique may be useful in making the network learn
        right features, not something suprious.

        @param image A 3-d image
        @return a rotated image having the same shape
        """
        # rotate by angle \in (0, ~pi/2)
        #return tf.contrib.image.rotate(image, random.uniform(0, 1.5))
        return tf.image.rot90(image, tf.random_uniform(shape=[], minval=1, maxval=4, dtype=tf.int32))

    @staticmethod
    def flip(image):
        """
        Performs left-right and up-down flip. This technique may be useful
        in making the network learn right features, not something suprious.

        @param image A 3-d image
        @return a flipped image having the same shape
        """
        rand_val = random.random()
        if rand_val < 0.66:
            image = tf.image.flip_left_right(image)
        if rand_val > 0.33:
            image = tf.image.flip_up_down(image)
        return image

    @staticmethod
    def color(image):
        """
        Applies color effects. This augmentation may be useful in
        making the network invariant to luminance.

        @param image A 3-d image
        @return an image having the same shape
        """
        #image = tf.image.random_hue(image, 0.03)
        #image = tf.image.random_saturation(image, 0.4, 0.9)
        image = tf.image.random_brightness(image, 0.05)
        #image = tf.clip_by_value(image, 0.0, 255.0)
        image = tf.image.random_contrast(image, 0.8, 1.1)
        image = tf.clip_by_value(image, 0.0, 255.0)
        return image


    @staticmethod
    def zoom(image):
        """
        Zoom augmentation. It may be useful in making the network
        learn scale-invariant features

        @param image a 3-d image
        @return augmented image
        """
        # Generate crop settings, ranging from a 5% to 15% crop.
        scales = list(np.arange(0.85, 0.95, 0.01))
        boxes = np.zeros((len(scales), 4))

        for i, scale in enumerate(scales):
            x1 = y1 = 0.5 - (0.5 * scale)
            x2 = y2 = 0.5 + (0.5 * scale)
            boxes[i] = [x1, y1, x2, y2]

        def random_crop(img):
            # Create different crops for an image
            crops = tf.image.crop_and_resize([img], boxes=boxes, box_ind=np.zeros(len(scales)), crop_size=(64, 64))
            # Return a random crop
            return crops[tf.random_uniform(shape=[], minval=0, maxval=len(scales), dtype=tf.int32)]

        return tf.cast(random_crop(image), tf.float32)


def augment_image(image, factor):
    how_often = {'zoom': 0.7, 'color': 0.5, 'flip': 0.5, 'rotate': 0.5}
    #how_often = {'zoom': 0.5, 'color': 0.5, 'flip': 0.5, 'rotate': 0.5}
    ops = list(how_often.keys())
    random.shuffle(ops)
    apply_flag = lambda aug: np.random.binomial(1, how_often[aug])
    proc_imgs = []
    
    curr_img = tf.identity(image)
    proc_imgs.append(Augmentors.zoom(curr_img))
    curr_img = tf.identity(image)
    proc_imgs.append(Augmentors.color(curr_img))
    curr_img = tf.identity(image)
    proc_imgs.append(Augmentors.flip(curr_img))
    curr_img = tf.identity(image)
    proc_imgs.append(Augmentors.rotate(curr_img))

    """
    for i in range(factor):
        curr_img = tf.identity(image)
        for k in ops:
            if apply_flag(k):
                curr_img = getattr(Augmentors, k)(curr_img)
        proc_imgs.append(curr_img)
    """
    return proc_imgs


def generate_dataset(elem, factor):
    img = elem['features']
    proc_imgs = augment_image(img, factor)
    dataset = tf.data.Dataset.from_tensor_slices(proc_imgs)
    dataset = dataset.map(lambda x: {'id': elem['id'], 'features': x, 'label': elem['label']})
    return dataset


def augment_dataset(dataset, factor=1):
    """
    @param dataset Raw dataset
    @factor float For each image in the dataset it generates `factor`
    number of images on average
    """
    # calculates multiplicity - the number of processed images
    # generated from a given raw image
    multi = lambda: (int(factor) + np.random.binomial(1, factor % 1))
    augmented = dataset.flat_map(lambda elem: generate_dataset(elem, multi()))
    return augmented


def write_image(imarr, loc):
    arr = imarr.astype('uint8')
    im = Image.fromarray(arr).convert('RGB')
    write_path = 'render/img_{}.jpg'.format(loc)
    im.save(write_path)


def write_dataset(dataset, filepath):
    iter = dataset.make_one_shot_iterator()
    with open(filepath, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                               quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["id", *["feat%s" % i for i in range(12288)], "label"])
        for elem in iter:
            id = int(elem['id'].numpy())
            label = int(tf.argmax(elem['label'], axis=0).numpy())
            row = [id] + list(elem['features'].numpy().astype('uint8').ravel()) + [label]
            writer.writerow(row)


def to_ndarr(dataset):
    batch = dataset.batch(50000)
    data_tensors = tf.data.experimental.get_single_element(batch)
    with tf.Session() as sess:
        arr = sess.run(data_tensors)
        if 'label' in arr:
            return arr['features'], arr['label']
        else:
            return arr['features']


def compute_stats(X):
    return (np.mean(X, axis=0), np.std(X, axis=0))

def standardize(X, mean, std):
    return (X - mean) / std


if __name__ == '__main__':
    dataset = load_train('./data/train_trunc1.csv')
    augmented = augment_dataset(dataset, factor=4)
    combined = dataset.concatenate(augmented)
    write_dataset(combined, './data/train_combined_1.csv')
    # augmented = augment_dataset(dataset, factor=5)
    # # print(dataset.output_shapes)
    # iter_raw = dataset.make_one_shot_iterator()
    #iter_aug = augmented.make_one_shot_iterator()

    i = 0
    for e in iter_aug:
        write_image(e["features"].numpy(), "proc" + str(i))
        i = i + 1
        if i == (12 * 4):
            break
    # X, Y = to_ndarr(dataset)
    # mean, std = compute_stats(X)
    # X = standardize(X, mean, std)
    # print(X)
    # print(Y)
    # print(X.shape)
    # print(Y.shape)

    # el1 = dataset.make_one_shot_iterator().get_next()
    # el2 = augmented.make_one_shot_iterator().get_next()
    # with tf.Session() as sess:
    #     for i in range(5):
    #         val1 = sess.run(el1)
    #         val2 = sess.run(el2)
    #         write_image(val1["features"], "raw" + str(i))
    #         write_image(val2["features"], "proc" + str(i))
