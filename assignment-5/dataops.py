"""
Copyright © Divyanshu Kakwani, Diptanshu Kakwani 2019, all rights reserved.
"""

import csv
import numpy as np

from functools import partial


def load_csv(path):
    """
    Loads csv and pre-processes it
    """
    with open(path, 'r') as file:
        reader = csv.reader(file, delimiter=',')
        # remove the header and return
        return list(reader)[1:]


def np_zero_pad(seqs):
    max_len = max(len(seq) for seq in seqs)
    return np.array([seq + [0] * (max_len - len(seq)) for seq in seqs])


def preprocess_word(word):
    """
    Remove whitespaces and insert start and end markers
    """
    return '^' + word.replace(' ', '') + '$'


def make_batch_iterator(inp_arr, batch_size=32):
    bidx = 0
    while True:
        start = (bidx * batch_size)
        end = ((bidx + 1) * batch_size)
        if start >= inp_arr.shape[0]:
            break
        yield inp_arr[start:end, :]
        bidx += 1


def write_subm(filename, **kwargs):
    inp = kwargs.get('inp', None)
    targ = kwargs['targ']
    src_cidx = kwargs.get('src_cidx', None)
    targ_cidx = kwargs['targ_cidx']

    targ = [list(seq) for seq in targ]

    # remove all elements after $, including $
    for i in range(len(targ)):
        end_mark = targ_cidx.sym2idx['$']
        targ[i] = targ[i] + [end_mark, 0]
        eff_end = min(targ[i].index(end_mark), targ[i].index(0))
        targ[i] = targ[i][:eff_end]

    targ = targ_cidx.to_symbols(targ)

    print("TARG: ", targ)

    with open(filename, 'w') as fp:
        print('id,HIN', file=fp)
        for i in range(len(targ)):
            print('{},{}'.format(i, ' '. join(char for char in targ[i] if char)), file=fp)


class CharIndex:
    """
    Builds character indices from a given corpus of characters.
    It can be used to convert character symbols to character
    indices and vice-versa
    """
    def __init__(self, char_corpus, max_size=None):
        """
        @param char_corpus A list of characters
        """
        # Identify all the unique characters
        self.uniq_chars = set(char_corpus)
        self.uniq_chars.add('*')    # Unknown symbols are mapped to *
        self.uniq_chars.add('^')    # Start marker
        self.uniq_chars.add('$')    # End marker

        # create lookup tables
        # start indexing from 1 - index 0 is reserved for pad character
        ordering = [(i+1, c) for i, c in enumerate(self.uniq_chars)]
        self.sym2idx = {c: i for i, c in ordering}
        self.idx2sym = {i: c for i, c in ordering}

        # Custom dict lookup function
        self.get = lambda map: (lambda key: map.get(key) or map.get('*'))

    def to_symbols(self, index_matrix):
        nb_seqs = len(index_matrix)
        symbol_matrix = [None] * nb_seqs
        for i in range(nb_seqs):
            symbol_matrix[i] = list(map(self.get(self.idx2sym), index_matrix[i]))
        return symbol_matrix

    def to_indices(self, symbol_matrix):
        nb_seqs = len(symbol_matrix)
        index_matrix = [None] * len(symbol_matrix)
        for i in range(len(symbol_matrix)):
            index_matrix[i] = list(map(self.get(self.sym2idx), symbol_matrix[i]))
        return index_matrix

    def num_chars(self):
        return len(self.uniq_chars)



