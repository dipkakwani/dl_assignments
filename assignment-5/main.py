"""
Copyright © Divyanshu Kakwani, Diptanshu Kakwani 2019, all rights reserved.
"""

import argparse
import sys
import enchant
import itertools
import numpy as np
import tensorflow as tf
from dataops import *
from model import TranslitModel

def parse_args():
    """
    Parses and processes the arguments obtained from the command
    lines and returns it as a dictionary.
    """
    parser = argparse.ArgumentParser()
    req_named_args = parser.add_argument_group("required named arguments")
    req_named_args.add_argument("--lr", required=True)
    req_named_args.add_argument("--batch_size", required=True)
    req_named_args.add_argument("--init", required=True)
    req_named_args.add_argument("--save_dir", required=True)
    req_named_args.add_argument("--epochs", required=True)
    req_named_args.add_argument("--train", required=True)
    req_named_args.add_argument("--val", required=True)
    req_named_args.add_argument("--test", required=True)
    req_named_args.add_argument("--dropout_prob", required=True)
    req_named_args.add_argument("--decode_method", required=True)
    req_named_args.add_argument("--beam_width", required=False)
    req_named_args.add_argument("--pretrain", required=False)
    req_named_args.add_argument("--num_units", required=True)

    args = vars(parser.parse_args())

    # Cast the arguments into appropriate types
    args["lr"]           = float(args["lr"])
    args["batch_size"]   = int(args["batch_size"])
    args["init"]         = int(args["init"])
    args["epochs"]       = int(args["epochs"])
    args["dropout_prob"] = float(args["dropout_prob"])
    args["pretrain"]     = (args["pretrain"] == "True" or args["pretrain"] == "true")
    args["num_units"]    = int(args["num_units"])

    return args


checker = enchant.Dict('en-US')

def check(word):
    eng = True
    for w in word.split('_'):
        if not checker.check(w):
            eng = False
    return eng


if __name__ == '__main__':
    gparams = parse_args()
    checker = enchant.Dict('en-US')


    train_data = load_csv(gparams['train'])
    val_data = load_csv(gparams['val'])
    test_data = load_csv(gparams['test'])

    # Generate train data
    train_inp_words  = [datum[1].replace(' ', '') for datum in train_data]
    train_targ_words = [datum[2].replace(' ', '') + '$' for datum in train_data]

    #train_inp_words = [('#' if check(word) else '@') + word for word in train_inp_words]

    # words in input_words look like "^COMPUTER$"

    # generate character indices
    src_cidx = CharIndex(itertools.chain(*train_inp_words))
    target_cidx = CharIndex(itertools.chain(*train_targ_words))

    # convert characters to indices and create zero-padded np arrays
    train_inp = np_zero_pad(src_cidx.to_indices(train_inp_words))
    train_targ = np_zero_pad(target_cidx.to_indices(train_targ_words))

    # Generate validation data
    val_inp_words = [datum[1].replace(' ', '') for datum in val_data]
    val_targ_words = [datum[2].replace(' ', '') + '$' for datum in val_data]

    #val_inp_words = [('#' if check(word) else '@') + word for word in val_inp_words]

    # convert characters to indices and create zero-padded np arrays
    val_inp = np_zero_pad(src_cidx.to_indices(val_inp_words))
    val_targ = np_zero_pad(target_cidx.to_indices(val_targ_words))

    # Setup and train the model
    model = TranslitModel(src_cidx, target_cidx, **gparams)
    model.train(train_inp, train_targ, val_inp, val_targ, **gparams)

    # Generate prediction on the test data
    test_inp_words = [datum[1].replace(' ', '') for datum in test_data]
    #test_inp_words = [('#' if check(word) else '@') + word for word in test_inp_words]
    test_inp = np_zero_pad(src_cidx.to_indices(test_inp_words))

    test_preds = model.test(test_inp, train_targ.shape[1])
    write_subm("predictions.csv", targ_cidx=target_cidx, targ=test_preds.T)
