#!/usr/bin/env bash

python3 main.py --lr 0.01 \
                 --batch_size 64 \
                 --epochs 20 --save_dir pa2/ \
                 --train data/train_trunc.csv \
                 --val data/valid.csv --test data/partial_test_400.csv \
                 --init 2 --pretrain false \
                 --num_units 128 --dropout_prob 0.8 --decode_method 0

