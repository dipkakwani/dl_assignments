"""
Copyright Divyanshu Kakwani, Diptanshu Kakwani 2019, all rights reserved.
"""
import tensorflow as tf
import numpy as np

from dataops import make_batch_iterator

"""
Assorted Notes
================

1. Time major determines the order of dimensions of the output. If set
to true, the first dimension becomes time

2. To be able to predict variable length sequences, we need to use start
and end markers

3. There is no explicit output of an RNN. We have to use an FC layer
on top of an RNN to get the output

4. Teacher forcing is the act of using the ground truth as the input for
each time step, rather than the output of the network



Adding sentinel tokens
========================
We don't add any sentinel token in the input sequences. In the output
sequences, we add the character $ to mark the end of a sequence. This
helps in predicting variable length sequences

"""


class TranslitModel:
    """
    A seq to seq model using an encoder-decoder architecture
    """
    def __init__(self, src_vocab, target_vocab, **params):
        self.inembed_size      = 512
        self.encoder_units     = 256
        self.decoder_units     = 512
        self.outembed_size     = 512
        self.src_vocab         = src_vocab
        self.src_vocab_size    = src_vocab.num_chars() + 1
        self.target_vocab      = target_vocab
        self.target_vocab_size = target_vocab.num_chars() + 1
        self.params            = params
        self.save_path         = None
        self.session           = tf.Session()

        self.encoder = Encoder(self.encoder_units, self.src_vocab_size, self.inembed_size)
        self.decoder = Decoder(self.decoder_units, self.target_vocab_size, self.outembed_size)

    def train(self, train_inp, train_targ, val_inp, val_targ, **opts):

        batch_size = opts['batch_size']
        lr         = opts['lr']
        max_epoch  = opts['epochs']
        train_inp  = np.copy(train_inp)
        train_targ  = np.copy(train_targ)

        # Build the computational graph
        seq_tensor  = tf.placeholder(tf.int32, (None, None))
        targ_tensor = tf.placeholder(tf.int32, (None, None))
        batch_loss  = self.train_step(seq_tensor, targ_tensor, train_targ.shape[1])
        train_preds_tensor = self.predict(seq_tensor, train_targ.shape[1])
        val_preds_tensor   = self.predict(seq_tensor, val_targ.shape[1])
        valid_loss = self.valid_loss(seq_tensor, targ_tensor, val_targ.shape[1])

        # Setup optimizer
        params = tf.trainable_variables()
        gradients = tf.gradients(batch_loss, params)
        max_gradient_norm = 5.0
        clipped_gradients, _ = tf.clip_by_global_norm(gradients, max_gradient_norm)
        optimizer = tf.train.AdamOptimizer(learning_rate=lr)
        update_step = optimizer.apply_gradients(zip(clipped_gradients, params))

        best_acc = 0
        patience = 5


        saver = tf.train.Saver()
        init = tf.global_variables_initializer()


        with self.session.as_default():
            self.session.run(init)
            for epoch in range(max_epoch):
                total_loss = 0
                # Shuffling
                p = np.random.permutation(len(train_inp))
                train_inp, train_targ = train_inp[p], train_targ[p]
                inp_batches  = make_batch_iterator(train_inp, batch_size)
                targ_batches = make_batch_iterator(train_targ, batch_size)
                for (bid, (inp, targ)) in enumerate(zip(inp_batches, targ_batches)):
                    # convert to time major
                    inp, targ = inp.T, targ.T
                    loss, _ = self.session.run([batch_loss, update_step], \
                                               feed_dict={seq_tensor: inp, \
                                                          targ_tensor: targ})
                    total_loss += loss


                train_preds    = self.session.run(train_preds_tensor, {seq_tensor: train_inp.T})
                train_accuracy = self.compute_accuracy(train_targ.T, train_preds)
                val_preds      = self.session.run(val_preds_tensor, {seq_tensor: val_inp.T})
                #val_loss       = self.session.run(valid_loss, {seq_tensor: val_inp.T, \
                #                                               targ_tensor: val_targ.T})
                val_accuracy   = self.compute_accuracy(val_targ.T, val_preds)

                print('Epoch {} Train Loss {:.4f}'\
                      .format(epoch + 1, total_loss / batch_size))
                print("Train accuracy {:.6f} Validation accuracy {:.6f}".\
                      format(train_accuracy * 100, val_accuracy * 100))


                if val_accuracy > best_acc:
                    self.save_path = saver.save(self.session,
                                                self.params["save_dir"] +
                                                "model-" + str(epoch) + ".ckpt")
                    patience = 5
                    best_acc = val_accuracy
                else:
                    patience -= 1
                    if patience <= 0 and i >= 10:
                        break


            saver.restore(self.session, self.save_path)


    def compute_accuracy(self, targ, pred):
        accuracy = 0
        accuracy = np.count_nonzero(targ == pred)
        return accuracy/(targ.shape[0] * targ.shape[1])

    def loss_func(self, targ, pred):
        mask = tf.math.logical_not(tf.math.equal(targ, 0))
        loss_ = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=targ, logits=pred)
        mask = tf.cast(mask, dtype=loss_.dtype)
        loss_ += mask

        return tf.reduce_mean(loss_)

    def train_step(self, seq, targ, max_time):
        """
        Trains the network.

        @param seq [max_time, batch_size]
        @param targ [max_time, batch_size]
        """
        keep_prob = tf.constant(1)
        enc_outputs, final_state = self.encoder(seq, keep_prob)
        dec_hidden = (final_state, final_state) # For 2-layered decoder
        #dec_hidden = final_state

        # Insert start character to pass it as the initial input to decoder
        dec_input = tf.fill([tf.shape(seq)[1]], self.target_vocab.sym2idx['^'])

        drop_rate = tf.constant(self.params["dropout_prob"])

        # use teacher forcing
        loss = 0
        for t in range(max_time):
            pred, dec_hidden = self.decoder(dec_input, dec_hidden, enc_outputs, drop_rate)
            loss += self.loss_func(targ[t], pred)
            dec_input = targ[t]

        batch_loss = (loss / max_time)
        return batch_loss

    def test(self, test_inp, max_time, pretrain=False):
        if pretrain:
            saver.restore(self.session, tf.train.latest_checkpoint(self.params["save_dir"]))
        seq = tf.placeholder(tf.int32, (None, None))
        # all_vars = set(tf.all_variables())
        test_preds = self.predict(seq, max_time)
        # self.session.run(tf.initialize_variables(set(tf.all_variables()) - all_vars))
        test_preds = self.session.run(test_preds, {seq: test_inp.T})
        return test_preds


    def predict(self, seq, max_time):
        """
        returns prediction tensor of shape [max_time, batch_size] obtained
        by feeding the network `seq` tensor. Each prediction is a sequence
        of character indices

        @param seq [max_time, batch_size]
        """
        keep_prob = tf.constant(1.0)
        # Pass through encoder
        enc_outputs, final_state = self.encoder(seq, keep_prob)
        dec_hidden = (final_state, final_state)
        #dec_hidden = final_state

        # Pass through decoder
        dec_input = tf.fill([tf.shape(seq)[1]], self.target_vocab.sym2idx['^'])
        preds = [None] * max_time

        drop_rate = tf.constant(0)
        for t in range(max_time):
            preds[t], dec_hidden = self.decoder(dec_input, dec_hidden, enc_outputs, drop_rate)
            preds[t] = tf.argmax(preds[t], axis=1)
            dec_input = preds[t]
            preds[t] = tf.expand_dims(preds[t], 0)

        return tf.concat(preds, axis=0)

    def valid_loss(self, seq, targ, max_time):
        """
        returns prediction tensor of shape [max_time, batch_size] obtained
        by feeding the network `seq` tensor. Each prediction is a sequence
        of character indices

        @param seq [max_time, batch_size]
        """
        keep_prob = tf.constant(1.0)
        # Pass through encoder
        enc_outputs, final_state = self.encoder(seq, keep_prob)
        dec_hidden = (final_state, final_state)
        #dec_hidden = final_state
        loss = 0

        # Pass through decoder
        dec_input = tf.fill([tf.shape(seq)[1]], self.target_vocab.sym2idx['^'])
        preds = [None] * max_time

        drop_rate = tf.constant(0)
        for t in range(max_time):
            preds[t], dec_hidden = self.decoder(dec_input, dec_hidden, enc_outputs, drop_rate)
            loss += self.loss_func(targ[t], preds[t])
            preds[t] = tf.argmax(preds[t], axis=1)
            dec_input = preds[t]
            preds[t] = tf.expand_dims(preds[t], 0)


        batch_loss = (loss / max_time)
        return batch_loss


class Encoder:
    """
    Given a sequence of indices of length N, it computes:
        (1) a "state vector" that encodes the information
            about the entire sequence.
        (2) N outputs
    """

    def __init__(self, state_size, vocab_size, embed_size):
        """
        create vars and RNN cells
        """
        self.embed_param = tf.get_variable(
            "encoder_embed_param", [vocab_size, embed_size], dtype=tf.float32)
        self.fw_cell = tf.nn.rnn_cell.BasicLSTMCell(state_size)
        self.bw_cell = tf.nn.rnn_cell.BasicLSTMCell(state_size)
        

    def length(self, seq_batch):
        """
        convert positive numbers to 1 and then count the number of ones

        @param seq_batch: [max_time, batch_size]
        """
        used = tf.sign(seq_batch)
        length = tf.reduce_sum(used, 0)
        return length

    def __call__(self, seq_batch, keep_prob):
        """
        computes (outputs, final_state) given a batch of sequences

        @param seq_batch: [max_time, batch_size]
        """
        self.fw_cell = tf.nn.rnn_cell.DropoutWrapper(self.fw_cell, \
                                                     output_keep_prob=keep_prob)
        self.bw_cell = tf.nn.rnn_cell.DropoutWrapper(self.bw_cell, \
                                                     output_keep_prob=keep_prob)

        # Dimension of seq_lengths: [batch_size]
        seq_lengths = self.length(seq_batch)

        # Dimensions of embeds: [max_time, batch_size, embedding_size]
        embeds = tf.nn.embedding_lookup(self.embed_param, seq_batch)

        """
        # To use uni-directional rnn, set encoder_units=decoder_units
        outputs, final_state = tf.nn.dynamic_rnn(
            self.fw_cell,
            embeds,
            sequence_length=seq_lengths,
            dtype=tf.float32,
            time_major=True)
        """
        bd_outputs, bd_final_states = tf.nn.bidirectional_dynamic_rnn(
            self.fw_cell,
            self.bw_cell,
            embeds,
            sequence_length=seq_lengths,
            dtype=tf.float32,
            time_major=True)
        # Dimensions of outputs: [max_time, batch_size, num_units]
        # Dimensions of final_state: [batch_size, num_units]

        outputs = tf.concat(bd_outputs, -1)
        fw_final_state, bw_final_state = bd_final_states
        final_state_h = tf.concat([fw_final_state.h, bw_final_state.h], -1)
        final_state_c = tf.concat([fw_final_state.c, bw_final_state.c], -1)
        final_state = (final_state_c, final_state_h)

        return (outputs, final_state)


class BahdanauAttention:

    def __init__(self, state_size):
        self.W1 = tf.layers.Dense(state_size)
        self.W2 = tf.layers.Dense(state_size)
        self.V = tf.layers.Dense(1)


    def __call__(self, hidden, enc_outputs, drop_rate):
        """
        @param hidden [batch_size, state_size]
        @param enc_outputs [batch_size, max_time, state_size]

        returns context vector that is built by attending appropriate
        time steps in enc_outpus that are relevant to hidden
        DIMS:
        hidden [batch_size, state_size]
        enc_outputs [max_time, batch_size, state_size]

        """
        # hidden shape == (1, batch_size, state_size)
        # we are doing this to perform addition to calculate the score
        hidden = tf.expand_dims(hidden, 0)
        W1Drop = tf.layers.dropout(self.W1(enc_outputs), drop_rate)
        W2Drop = tf.layers.dropout(self.W2(hidden), drop_rate)

        score = self.V(tf.nn.tanh(W1Drop + W2Drop))

        # attention_weights shape == (batch_size, max_length, 1)
        # we get 1 at the last axis because we are applying score to self.V
        attention_weights = tf.nn.softmax(score, axis=1)

        # context_vector shape after sum == (batch_size, hidden_size)
        context_vector = attention_weights * enc_outputs
        context_vector = tf.reduce_sum(context_vector, axis=0)

        return context_vector


class Decoder:

    def __init__(self, state_size, vocab_size, embed_size):
        self.embed_param = tf.get_variable("decoder_embed_param", [vocab_size, embed_size])
        #self.cell = tf.nn.rnn_cell.BasicLSTMCell(state_size)
        self.cell = tf.nn.rnn_cell.MultiRNNCell([tf.nn.rnn_cell.BasicLSTMCell(state_size) for _ in range(2)])
        self.attention = BahdanauAttention(state_size)
        self.fc = tf.layers.Dense(vocab_size, activation=tf.nn.relu)

    def __call__(self, input, hidden, enc_outputs, drop_rate):
        """
        Represents call to a single cell. The call recevies indices as
        input, previous state and encoder outputs. It returns predictions,
        in the form of one hot vectors



        @param input is a batch of indices corresponding to a
               single time step. Shape: [batch_size]

        @returns pred_batch


        Dimensions of encoder_outputs: [max_time, batch_size, state_size]
        Dimensions of final_state: [batch_size, state_size]
        """

        input = tf.nn.embedding_lookup(self.embed_param, input)

        context_vector = self.attention(hidden[0][1], enc_outputs, drop_rate)

        # x shape after concatenation == (batch_size, 1, embed_dim + hidden_size)
        # input = tf.concat([tf.expand_dims(context_vector, 1), input], axis=-1)
        input = tf.concat([context_vector, input], axis=-1)

        # passing the concatenated vector to the GRU
        output, state = self.cell(input, hidden)

        # output shape == (batch_size * 1, hidden_size)
        # output = tf.reshape(output, (-1, output.shape[2]))

        # output shape == (batch_size * 1, vocab)
        y = self.fc(output)

        return y, state
